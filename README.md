# JavaScript/TypeScript CI Image

[![pipeline status](https://gitlab.com/usvc/images/ci/node/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/ci/node/commits/master)
[![dockerhub link](https://img.shields.io/badge/dockerhub-usvc%2Fci--node-blue.svg)](https://hub.docker.com/r/usvc/ci-node)

A CI image for use in `usvc` projects written in JavaScript/TypeScript.

# Usage

## GitLab CI

### Standard Usage

Use the image by specifying the `job.image` value as `"usvc/ci-node:latest"`:

```yaml
job_name:
  stage: stage_name
  image: usvc/ci-node:latest
  script:
    - ...
```

### With Docker-in-Docker (`dind`)

Use the image by specifying the `job.image` value as `"usvc/ci-node:gitlab-latest"`. You'll also need to specify that you're using the `docker:dind` service.

```yaml
job_name:
  stage: stage_name
  image: usvc/ci-node:gitlab-latest
  services:
    - docker:dind
  script:
    - ...
```

# Development Runbook

## CI Configuration

Set the following variables in the environment variable configuration of your GitLab CI:

| Key | Value |
| ---: | :--- |
| DOCKER_REGISTRY_URL | URL to your Docker registry |
| DOCKER_REGISTRY_USER | Username for your registry user |
| DOCKER_REGISTRY_PASSWORD | Password for your registry user |

# License

Content herein is licensed under the [MIT license](./LICENSE).
